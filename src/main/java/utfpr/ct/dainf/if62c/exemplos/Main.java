/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.exemplos;

/**
 *
 * @author pozza
 */
public class Main {

    public static void exibir(Matriz m) {
        for (int i = 0; i < m.getMatriz().length; ++i) {
            for (int j = 0; j < m.getMatriz()[i].length; ++j) {         
                System.out.printf("%d\t", m.getMatriz()[i][j]);
            }
            System.out.printf("\n");
        }
    }

    public static void main(String[] args) {
        Matriz ex = new Matriz(2, 2);
        Matriz ex2 = new Matriz(2, 2);
        for (int i = 0; i < ex.getMatriz().length; i++) {
            for (int j = 0; j < ex.getMatriz().length; j++) {
                ex.getMatriz()[i][j] = 2;
                ex2.getMatriz()[i][j] = 2;
            }
        }
        exibir(ex);
        ex = ex.soma(ex);
        exibir(ex);
        ex2=ex2.prod(ex2);
    }

}
